using System.Collections.Generic;
using System.Threading.Tasks;
using OtusDbApplication.Core.StorageManagers.Interfaces;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Core.StorageManagers
{
    public class StudentsStorageManager : IStudentsStorageManager
    {
        private readonly IUnitOfWork _uow;

        public StudentsStorageManager(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Task<IReadOnlyList<Student>> GetAsync()
            => _uow.StudentsRepository.GetAsync();

        public Task<int> CreateAsync(Student student)
            => _uow.StudentsRepository.CreateAsync(student);

        public Task<int> UpdateAsync(Student student)
            => _uow.StudentsRepository.UpdateAsync(student);

        public Task<int> DeleteAsync(long id)
            => _uow.StudentsRepository.DeleteAsync(id);
    }
}