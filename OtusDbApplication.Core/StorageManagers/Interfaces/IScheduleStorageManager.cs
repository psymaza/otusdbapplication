using System.Collections.Generic;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Core.StorageManagers.Interfaces
{
    public interface IScheduleStorageManager
    {
        Task<IList<Schedule>> Get();
        Task Add(Schedule schedule);
        Task Update(Schedule schedule);
        Task Delete(long id);
    }
}