using System.Collections.Generic;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Core.StorageManagers.Interfaces
{
    public interface IStudentsStorageManager
    {
        Task<IReadOnlyList<Student>> GetAsync();
    }
}