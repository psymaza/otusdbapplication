﻿using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Microsoft.Extensions.DependencyInjection;
using OtusDbApplication.Data.Migrations;

namespace OtusDbApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddPostgres()
                    .WithGlobalConnectionString(
                        "Host=localhost;port=5432;Database=otus;Username=otus;Password=otus;")
                    .ScanIn(typeof(CreateTables).Assembly).For.All())
                .Configure<RunnerOptions>(config => { config.Profile = "InitialData"; })
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                // Start of type filter configuration
                // .Configure<RunnerOptions>(opt => {
                // opt.Tags = new[] { "UK", "Production" }
                // })
                .BuildServiceProvider(false);

            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();
            runner.ListMigrations();
            // runner.MigrateDown(0);
            runner.MigrateUp();
        }
    }
}