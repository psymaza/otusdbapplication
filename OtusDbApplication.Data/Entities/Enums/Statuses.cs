namespace OtusDbApplication.Data.Entities.Enums
{
    public enum Statuses
    {
        Planned,
        During,
        Completed,
        Postponed
    }
}