namespace OtusDbApplication.Data.Entities.Enums
{
    public enum Roles
    {
        Student,
        Teacher,
        Moderator
    }
}