using OtusDbApplication.Data.Entities.Interfaces;

namespace OtusDbApplication.Data.Entities.BaseEnteties
{
    public class BaseEntity : IEntity<long>
    {
        public long Id { get; set; }
    }
}