using System.Collections.Generic;
using OtusDbApplication.Data.Entities.BaseEnteties;

namespace OtusDbApplication.Data.Entities
{
    public class Lesson : Lookup
    {
        public IEnumerable<Course> Courses { get; set; }
    }
}