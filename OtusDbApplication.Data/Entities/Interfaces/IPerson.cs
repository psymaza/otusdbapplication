using OtusDbApplication.Data.Entities.Enums;

namespace OtusDbApplication.Data.Entities.Interfaces
{
    public interface IPerson
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte? Age { get; set; }
        public Roles Role { get; }
    }
}