using System.Collections.Generic;
using OtusDbApplication.Data.Entities.BaseEnteties;
using OtusDbApplication.Data.Entities.Enums;
using OtusDbApplication.Data.Entities.Interfaces;

namespace OtusDbApplication.Data.Entities
{
    public class Teacher : BaseEntity, IPerson
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte? Age { get; set; }
        public Roles Role { get; } = Roles.Teacher;
        public IEnumerable<Specialization> Specializations { get; set; }
    }
}