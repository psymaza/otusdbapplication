using System;
using System.Collections.Generic;
using OtusDbApplication.Data.Entities.BaseEnteties;
using OtusDbApplication.Data.Entities.Enums;

namespace OtusDbApplication.Data.Entities
{
    public class Schedule : BaseEntity
    {
        public Lesson Lesson { get; set; }
        public Teacher Teacher { get; set; }
        public IEnumerable<Student> Students { get; set; }
        public DateTimeOffset Date { get; set; }
        public Statuses Status { get; set; }
    }
}