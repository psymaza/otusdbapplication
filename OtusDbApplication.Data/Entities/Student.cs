using System.Collections.Generic;
using OtusDbApplication.Data.Entities.BaseEnteties;
using OtusDbApplication.Data.Entities.Enums;
using OtusDbApplication.Data.Entities.Interfaces;

namespace OtusDbApplication.Data.Entities
{
    public class Student : BaseEntity, IPerson
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte? Age { get; set; }
        public Roles Role { get; } = Roles.Student;
        public IList<Course> Courses { get; }

        public Student()
        {
            Courses = new List<Course>();
        }
    }
}