using System;
using System.Data;
using Npgsql;
using OtusDbApplication.Data.Factories.Interfaces;

namespace OtusDbApplication.Data.Factories
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;
        public DbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }
        public IDbConnection CreateDbConnection()
            => !string.IsNullOrWhiteSpace(_connectionString)
                ? new NpgsqlConnection(_connectionString)
                : throw new ArgumentException(nameof(_connectionString));
    }
}