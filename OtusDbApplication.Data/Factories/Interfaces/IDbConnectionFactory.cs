using System.Data;

namespace OtusDbApplication.Data.Factories.Interfaces
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateDbConnection(); 
    }
}