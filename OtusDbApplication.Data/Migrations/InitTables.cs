using System;
using FluentMigrator;
using OtusDbApplication.Data.Entities.Enums;

namespace OtusDbApplication.Data.Migrations
{
    [Profile("InitialData")]
    public class InitTables : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("courses")
                .Row(new {name = "C# Professional"});

            Insert.IntoTable("specializations")
                .Row(new {name = ".Net Developer"});

            Insert.IntoTable("lessons")
                .Row(new {name = "Databases", course_id = 1})
                .Row(new {name = "ORMs", course_id = 1})
                .Row(new {name = "Reflection", course_id = 1});

            Insert.IntoTable("students")
                .Row(new
                {
                    name = "Иван", surname = "Иванович", role = Enum.GetName(typeof(Roles), Roles.Student),
                    course_id = 1
                })
                .Row(new
                {
                    name = "Роман", surname = "Романович", role = Enum.GetName(typeof(Roles), Roles.Student),
                    course_id = 1
                })
                .Row(new
                {
                    name = "Роман", surname = "Романович", role = Enum.GetName(typeof(Roles), Roles.Student),
                    course_id = 1
                })
                .Row(new
                {
                    name = "Роман", surname = "Романович", role = Enum.GetName(typeof(Roles), Roles.Student),
                    course_id = 1
                })
                .Row(new
                {
                    name = "Анастасия", surname = "Петровна", role = Enum.GetName(typeof(Roles), Roles.Student),
                    course_id = 1
                });

            Insert.IntoTable("teachers")
                .Row(new
                {
                    name = "Иван", surname = "Иванович", role = Enum.GetName(typeof(Roles), Roles.Teacher),
                    specialization_id = 1
                })
                .Row(new
                {
                    name = "Авраам", surname = "Руссо", role = Enum.GetName(typeof(Roles), Roles.Teacher),
                    specialization_id = 1
                })
                .Row(new
                {
                    name = "Александр", surname = "Пушкин", role = Enum.GetName(typeof(Roles), Roles.Teacher),
                    specialization_id = 1
                })
                .Row(new
                {
                    name = "Роман", surname = "Романович", role = Enum.GetName(typeof(Roles), Roles.Moderator),
                    specialization_id = 1
                })
                .Row(new
                {
                    name = "Анастасия", surname = "Петровна", role = Enum.GetName(typeof(Roles), Roles.Moderator),
                    specialization_id = 1
                });
        }

        public override void Down()
        {
            throw new System.NotImplementedException();
        }
    }
}