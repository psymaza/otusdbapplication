using System.Data;
using FluentMigrator;

namespace OtusDbApplication.Data.Migrations
{
    [Migration(1)]
    public class CreateTables : Migration
    {
        public override void Up()
        {
            Execute.Sql("CREATE TYPE role AS ENUM ('Teacher', 'Student', 'Moderator');");
            Execute.Sql("CREATE TYPE schedule_status AS ENUM ('planned', 'during', 'completed', 'postponed');");

            Create.Table("courses")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(100).NotNullable();

            Create.Table("specializations")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(100).NotNullable();

            Create.Table("lessons")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(100).NotNullable()
                .WithColumn("course_id").AsInt64().NotNullable()
                .Indexed("ix_lessons_course_id")
                .ForeignKey("fk_lessons_courses_course_id", "courses", "id");

            Create.Table("people")
                .WithColumn("id").AsInt64().PrimaryKey().Identity()
                .WithColumn("name").AsString(30).NotNullable()
                .WithColumn("surname").AsString(30).NotNullable()
                .WithColumn("age").AsInt16().Nullable()
                .WithColumn("role").AsCustom("role");

            Execute.Sql("CREATE TABLE teachers () INHERITS (people);");
            Execute.Sql("CREATE TABLE students () INHERITS (people);");

            Alter.Table("teachers")
                .AddColumn("specialization_id").AsInt64().NotNullable()
                .Indexed("ix_teachers_specialization_id")
                .ForeignKey("fk_teachers_specializations_specialization_id", "specializations", "id");

            Alter.Table("students")
                .AddColumn("course_id").AsInt64().NotNullable()
                .Indexed("ix_students_course_id")
                .ForeignKey("fk_students_courses_course_id", "lessons", "id");

            Create.Table("schedules")
                .WithColumn("teacher_id").AsInt64().NotNullable()
                .Indexed("ix_schedules_teacher_id")
                .ForeignKey("fk_schedules_teachers_teacher_id", "teachers", "id")
                .WithColumn("lesson_id").AsInt64().NotNullable()
                .Indexed("ix_schedules_lesson_id")
                .ForeignKey("fk_schedules_lessons_lesson_id", "lessons", "id")
                .WithColumn("student_id").AsInt64().NotNullable()
                .Indexed("ix_schedules_student_id")
                .ForeignKey("fk_schedules_students_student_id", "students", "id")
                .WithColumn("date").AsDateTimeOffset().NotNullable()
                .WithColumn("status").AsCustom("schedule_status");
        }

        public override void Down()
        {
            Delete.Table("teachers");
            Delete.Table("students");
            Delete.Table("people");
            Delete.Table("specializations");
            Delete.Table("lessons");
            Delete.Table("courses");
            Execute.Sql("DROP TYPE role;");
        }
    }
}