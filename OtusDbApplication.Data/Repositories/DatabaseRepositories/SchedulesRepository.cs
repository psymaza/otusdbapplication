using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class SchedulesRepository : BaseDatabaseRepository, ISchedulesRepository
    {
        public SchedulesRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        
        public Task<Schedule> GetAsync(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Schedule>> GetAsync(IEnumerable<long> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Schedule>> GetAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateAsync(Schedule entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(Schedule entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(long id)
        {
            throw new System.NotImplementedException();
        } 
    }
}