using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class TeachersRepository : BaseDatabaseRepository, ITeachersRepository
    {
        public TeachersRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public Task<Teacher> GetAsync(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Teacher>> GetAsync(IEnumerable<long> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Teacher>> GetAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateAsync(Teacher entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(Teacher entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(long id)
        {
            throw new System.NotImplementedException();
        }
    }
}