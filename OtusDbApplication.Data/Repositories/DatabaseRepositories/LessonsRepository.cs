using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class LessonsRepository : BaseDatabaseRepository, ILessonsRepository
    {
        public LessonsRepository(IDbTransaction transaction) : base(transaction)
        {
        }
        
        public Task<Lesson> GetAsync(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Lesson>> GetAsync(IEnumerable<long> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Lesson>> GetAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateAsync(Lesson entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(Lesson entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(long id)
        {
            throw new System.NotImplementedException();
        } 
    }
}