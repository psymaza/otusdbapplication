using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class CoursesRepository : BaseDatabaseRepository, ICoursesRepository
    {
        public CoursesRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public Task<Course> GetAsync(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Course>> GetAsync(IEnumerable<long> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Course>> GetAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateAsync(Course entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(Course entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(long id)
        {
            throw new System.NotImplementedException();
        }
    }
}