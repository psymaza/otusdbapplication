using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class StudentsRepository : BaseDatabaseRepository, IStudentsRepository
    {
        public StudentsRepository(IDbTransaction transaction) : base(transaction)
        {
        }

        public async Task<Student> GetAsync(long id)
        {
            var sql =
                @"
                    SELECT * FROM students s
                    JOIN courses c ON s.course_id = c.id
                    WHERE s.id = @value
                ";
            var result =
                await Connection.QueryAsync<Student, Course, Student>(sql, AddToCourse, new {value = id},
                    splitOn: "course_id");
            return result.SingleOrDefault();
        }

        public async Task<IReadOnlyList<Student>> GetAsync(IEnumerable<long> ids)
        {
            var sql =
                @"
                    SELECT * FROM students s
                    join courses c ON s.course_id = c.id
                    WHERE s.id = any (@values)
                ";
            var result =
                await Connection.QueryAsync<Student, Course, Student>(sql, AddToCourse, new {values = ids},
                    splitOn: "course_id");
            return result.ToImmutableList();
        }

        public async Task<IReadOnlyList<Student>> GetAsync()
        {
            var sql =
                @" SELECT * FROM students s 
                   join courses c ON s.course_id = c.id 
                ";
            var result =
                await Connection.QueryAsync<Student, Course, Student>(sql, AddToCourse, splitOn: "course_id");
            return result.ToImmutableList();
        }

        public async Task<int> CreateAsync(Student entity)
        {
            if (entity == null) return 0;

            var sql =
                @" INSERT INTO public.students(name, surname, age, role)
	                VALUES (@Name, @Surname, @Age, @Role);
                ";
            return await Connection.ExecuteAsync(sql, entity, Transaction);
        }

        public async Task<int> UpdateAsync(Student entity)
        {
            if (entity == null) return 0;

            var sql = @"
                UPDATE public.students
	                SET name = @Name, 
	                surname = @Surname, 
	                age = @Age, 
	                role = @Role, 
	                WHERE id = @Id;
            ";
            
            return await Connection.ExecuteAsync(sql, entity, Transaction);
        }

        public async Task<int> DeleteAsync(long id)
        {
            var sql =
                @"DELETE FROM students WHERE id = @id";
            return await Connection.ExecuteAsync(sql, new {id}, Transaction);
        }

        private Student AddToCourse(Student student, Course course)
        {
            student.Courses.Add(course);
            return student;
        }
    }
}