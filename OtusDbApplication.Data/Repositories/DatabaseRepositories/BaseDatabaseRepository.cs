using System.Data;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public abstract class BaseDatabaseRepository
    {
        protected IDbTransaction Transaction { get; }
        protected IDbConnection Connection => Transaction?.Connection;

        protected BaseDatabaseRepository(IDbTransaction transaction)
        {
            Transaction = transaction;
        }
    }
}