using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using OtusDbApplication.Data.Entities;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories.DatabaseRepositories
{
    public class SpecializationsRepository :  BaseDatabaseRepository, ISpecializationsRepository
    {
        public SpecializationsRepository(IDbTransaction transaction) : base(transaction)
        {
            
        }
        
        public Task<Specialization> GetAsync(long id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Specialization>> GetAsync(IEnumerable<long> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task<IReadOnlyList<Specialization>> GetAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> CreateAsync(Specialization entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> UpdateAsync(Specialization entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> DeleteAsync(long id)
        {
            throw new System.NotImplementedException();
        }
    }
}