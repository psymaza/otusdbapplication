using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface ITeachersRepository : IGenericRepository<Teacher, long>
    {
    }
}