using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface ILessonsRepository : IGenericRepository<Lesson, long>
    {
        
    }
}