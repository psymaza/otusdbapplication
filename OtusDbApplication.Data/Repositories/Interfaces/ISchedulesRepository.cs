using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface ISchedulesRepository : IGenericRepository<Schedule, long>
    {
        
    }
}