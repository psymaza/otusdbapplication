using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface IStudentsRepository : IGenericRepository<Student, long>
    {
        
    }
}