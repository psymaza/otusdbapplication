using System;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICoursesRepository CoursesRepository { get; }
        ILessonsRepository LessonsRepository { get; }
        ISchedulesRepository SchedulesRepository { get; }
        ISpecializationsRepository SpecializationsRepository { get; }
        IStudentsRepository StudentsRepository { get; }
        ITeachersRepository TeachersRepository { get; }
        void Commit();
    }
}