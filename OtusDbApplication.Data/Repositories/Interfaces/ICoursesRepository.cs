using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface ICoursesRepository : IGenericRepository<Course, long>
    {
    }
}