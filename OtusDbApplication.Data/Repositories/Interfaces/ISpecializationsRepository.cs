using OtusDbApplication.Data.Entities;

namespace OtusDbApplication.Data.Repositories.Interfaces
{
    public interface ISpecializationsRepository : IGenericRepository<Specialization, long>
    {
        
    }
}