using System;
using System.Data;
using OtusDbApplication.Data.Factories.Interfaces;
using OtusDbApplication.Data.Repositories.DatabaseRepositories;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private ICoursesRepository _coursesRepository;
        private ILessonsRepository _lessonsRepository;
        private ISchedulesRepository _schedulesRepository;
        private ISpecializationsRepository _specializationsRepository;
        private IStudentsRepository _studentsRepository;
        private ITeachersRepository _teachersRepository;

        public UnitOfWork(IDbConnectionFactory dbConnectionFactory)
        {
            _connection = dbConnectionFactory.CreateDbConnection();
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public ICoursesRepository CoursesRepository
            => _coursesRepository ??= new CoursesRepository(_transaction);

        public ILessonsRepository LessonsRepository
            => _lessonsRepository ??= new LessonsRepository(_transaction);

        public ISchedulesRepository SchedulesRepository
            => _schedulesRepository ??= new SchedulesRepository(_transaction);

        public ISpecializationsRepository SpecializationsRepository
            => _specializationsRepository ??= new SpecializationsRepository(_transaction);

        public IStudentsRepository StudentsRepository
            => _studentsRepository ??= new StudentsRepository(_transaction);

        public ITeachersRepository TeachersRepository
            => _teachersRepository ??= new TeachersRepository(_transaction);

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                ResetRepositories();
            }
        }

        private void ResetRepositories()
        {
            _coursesRepository = null;
            _lessonsRepository = null;
            _schedulesRepository = null;
            _specializationsRepository = null;
            _studentsRepository = null;
            _teachersRepository = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                ReleaseUnmanagedResources();
            }
        }

        private void ReleaseUnmanagedResources()
        {
            _transaction?.Dispose();
            _transaction = null;
            _connection?.Dispose();
            _connection = null;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}