using Microsoft.Extensions.DependencyInjection;

namespace OtusDbApplication.Console.Configuration
{
    public static class DapperConfiguration
    {
        public static IServiceCollection AddSnakeCaseMapping(this IServiceCollection services)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            return services;
        }
    }
}