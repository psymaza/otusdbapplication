using Microsoft.Extensions.DependencyInjection;
using OtusDbApplication.Core.StorageManagers;
using OtusDbApplication.Core.StorageManagers.Interfaces;
using OtusDbApplication.Data.Repositories.DatabaseRepositories;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Console.Configuration
{
    public static class StorageManagersConfiguration
    {
        public static IServiceCollection AddStorageManagers(this IServiceCollection services)
        {
            services.AddScoped<IStudentsStorageManager, StudentsStorageManager>();
            return services;
        }
    }
}