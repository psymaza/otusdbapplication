using Microsoft.Extensions.DependencyInjection;
using OtusDbApplication.Data.Factories;
using OtusDbApplication.Data.Repositories;
using OtusDbApplication.Data.Repositories.Interfaces;

namespace OtusDbApplication.Console.Configuration
{
    public static class RepositoryConfiguration
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            return services;
        }
    }
}