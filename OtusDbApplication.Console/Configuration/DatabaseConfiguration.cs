using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OtusDbApplication.Data.Factories;
using OtusDbApplication.Data.Factories.Interfaces;

namespace OtusDbApplication.Console.Configuration
{
    public static class DatabaseConfiguration
    {
        public static IServiceCollection AddDatabaseFactory(this IServiceCollection services,
            IConfiguration configuration)
        {
            var connectionString = configuration.GetSection("DefaultConnection").Value;
            // configuration.GetConnectionString("DefaultConnection");
            services.AddTransient<IDbConnectionFactory, DbConnectionFactory>(e =>
                new DbConnectionFactory(connectionString));
            return services;
        }
    }
}