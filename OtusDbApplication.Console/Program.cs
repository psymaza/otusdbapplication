﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OtusDbApplication.Console.Configuration;
using OtusDbApplication.Core.StorageManagers.Interfaces;
using OtusDbApplication.Data.Entities;
using Serilog;

namespace OtusDbApplication.Console
{
    class Program
    {
        private static readonly IServiceProvider _serviceProvider;
        private static readonly IConfiguration _configuration;

        static Program()
        {
            _configuration = GetServiceConfiguration();
            _serviceProvider = GetServiceProvider();
        }

        static int Main(string[] args)
        {
            try
            {
                MainAsync(args).Wait();
                return 0;
            }
            catch
            {
                return 1;
            }
        }

        static async Task MainAsync(string[] args)
        {
            Log.Information("Starting service");
            try
            {
                var studentsStorageManager = _serviceProvider.GetService<IStudentsStorageManager>();
                var students = await studentsStorageManager?.GetAsync();
                students.ToList().ForEach(e => Log.Debug(e.Name));

                Log.Information("Ending service");
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error running service");
                throw ex;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static IConfiguration GetServiceConfiguration()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
                .AddJsonFile("appsettings.json", false)
                .Build();
            return configuration;
        }

        private static IServiceProvider GetServiceProvider()
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console(Serilog.Events.LogEventLevel.Debug)
                .MinimumLevel.Debug()
                .Enrich.FromLogContext()
                .CreateLogger();

            var services = new ServiceCollection()
                .AddDatabaseFactory(_configuration)
                .AddSnakeCaseMapping()
                .AddRepositories()
                .AddStorageManagers();

            services.AddLogging();

            return services.BuildServiceProvider();
        }
    }
}